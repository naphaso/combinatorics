#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <iostream>
#include <string.h>
using namespace std;

void printb(long long vector, int start_pos) {
	int length = sizeof(long long)*8;
	
	assert (start_pos < length);

	for (int i=start_pos-1; i>=0; i--) {
		if (vector&(1ll<<i))
			cout << start_pos - i << " ";
	}

	cout << endl;
}

void reverse(char p[]) {
	size_t len=strlen(p);
	char t;
	for(size_t i=(--len), j=0; i>len/2; i--, j++)
		swap(p[i],p[j]);
}

unsigned int count_bits(long long n) {
  unsigned int c;
  for (c = 0; n; c++)
    n &= n - 1;
  return c;
}

void zakrevsky(int n, int k) {
	long long a = ( (1ll<<k)-1 )<<( n-k );

	printb(a, n);
	while ( a!=((1ll<<k)-1) ) {
		long long b = (a+1)&a;
		long long c = count_bits((b-1)^a)-2;
		a = ( ( ( ( (a+1)^a )<<1 )+1 )<<c )^b;
		printb(a, n);	
	}
}


int main(int argc, char * argv[])
{
	int n, k;
	cin >> n >> k;
	zakrevsky(n, k);
	return 0;
}

