#include <iostream>

using namespace std;

int main() {
  int n;
  cin >> n;
  
  int *a = new int[n];
  int *b = new int[n - 1];
  int m, j;
  
  // инициализация
  for (int i = 0; i < n; i++)
    a[i] = 0;
  for (int i = 0; i < n - 1; i++)
    b[i] = 1;
  m = 1;

  while (1) {

    // посещение
    for (int i = 0; i < n; i++)
      cout << (a[i] + 1) << " ";
    cout << endl;

    while (a[n - 1] != m) {
      a[n - 1]++;
      // посещение
      for (int i = 0; i < n; i++)
	cout << (a[i] + 1) << " ";
      cout << endl;
    }

    // поиск j
    j = n - 2;
    while (a[j] == b[j])
      j--;

    if (j == 0)
      break;
    a[j]++;

    // обнуление a_{j+1}...a_n
    m = b[j] + (a[j] == b[j]);
    j++;
    while (j < n - 1) {
      a[j] = 0;
      b[j] = m;
      j++;
    }
    a[n-1] = 0;
  }
  return 0;
}
