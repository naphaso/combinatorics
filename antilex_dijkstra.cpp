#include <iostream>

using namespace std;

inline void out(int *a, int n) {
  for(int i = 0; i < n; i++)
    cout << a[i] + 1<< " ";
  cout << endl;
}

int main() {
  int n;
  cin >> n;

  int *a = new int[n];
  for(int i = 0; i < n; i++)
    a[i] = i;

  int i = 1, j;
  while(i < n) {
    out(a, n);
    j = 0;
    while(a[i] < a[j]) j++;
    swap(a[i], a[j]);
    for(int l = 0, r = i - 1; l < r; l++, r--)
      swap(a[l], a[r]);
    for(i = 1; i < n && a[i] < a[i - 1]; i++);
  }
  out(a, n);
  
  return 0;
}
