#include <iostream>
#include <vector>

using namespace std;

int main(void) {
	int n, k, r;
	cin >> n >> k;

	vector<int> c;
	c.resize(k);
	for(int i = 0; i < k; i++) c[i] = i;


	while(true) {
		for(int i = 0; i < k; i++)
			cout << c[i] + 1 << " ";
		cout << endl;
		for(r = k - 1; r >= 0 && c[r] >= n - k + r; r--);
		if(r < 0) break;
		c[r]++;

		for(int j = 0; j < k - r; j++)
			c[r+j] = c[r] + j;
	}

	return 0;
}
