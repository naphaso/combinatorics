#include <iostream>
#include <vector>
#include <map>
#include <algorithm>

using namespace std;


int main() {
  int w, n;
  cin >> w >> n;
  int *a = new int[n];
  for(int i = 0; i < n; i++)
    cin >> a[i];

  map<int, vector<vector<int> > > ss;

  ss[0] = vector<vector<int> >();
  ss[0].push_back(vector<int>());

  for(int i = 0; i < n; i++) {
    for(map<int, vector<vector<int> > >::iterator it = ss.begin(); it != ss.end(); ++it) {
      if(it->first + a[i] <= w) {
	for(vector<vector<int> >::iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2) {
	  vector<int> sol = *it2;
	  if(find(sol.begin(), sol.end(), i) == sol.end()) {
	    sol.push_back(i);
	    ss[it->first + a[i]].push_back(sol);
	  }
	}
      }
    }
  }


  for(map<int, vector<vector<int> > >::iterator it = ss.begin(); it != ss.end(); it++) {
    cout << it->first << ": ";
    for(vector<vector<int> >::iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2) {
      cout << "{ ";
      for(vector<int>::iterator it3 = (*it2).begin(); it3 != (*it2).end(); ++it3)
	cout << *it3+1 << " ";
      cout << "} ";
    }
    cout << endl;
  }

  return 0;
}
