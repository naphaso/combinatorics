#include <iostream>
#include <list>
#include <vector>
using namespace std;

int w, n, *a;
vector<int> v;

void ksbt(int s, int d) {
  // stop condition
  if(d == n)
    return;
  // "yes" branch
  if(s + a[d] <= w) {
    s += a[d];
    v.push_back(d);

    ksbt(s, d+1);

    if(s == w) {
      for(vector<int>::iterator it = v.begin(); it != v.end(); ++it)
	cout << *it + 1 << " "; cout << endl;
    }
    
    v.pop_back();
    s -= a[d];
  }
  
  // "no" branch
  ksbt(s, d+1);
}

int main() {
  cin >> w >> n;
  a = new int[n];
  v.reserve(n);
  for(int i = 0; i < n; i++) cin >> a[i];
  
  ksbt(0, 0);

  return 0;
}
