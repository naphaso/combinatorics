#include <iostream>
#include <vector>
#include <set>
using namespace std;

int **mx;
//int f0;
int n;
vector<int> best_path;
int best_sum;

int F0(set<int> vset, int depth) {
  int a = 0, b = 0;
  
  for(int i = depth; i < n; i++) {
    int a_min = mx[i][0];
    for(set<int>::iterator jt = vset.begin(); jt != vset.end(); ++jt)
      if(mx[i][*jt] < a_min)
	a_min = mx[i][*jt];
    a += a_min;
  }

  for(set<int>::iterator jt = vset.begin(); jt != vset.end(); ++jt) {
    int b_min = mx[0][*jt];
    for(int i = depth; i < n; i++)
      if(mx[i][*jt] < b_min)
	b_min = mx[i][*jt];
    b += b_min;
  }

  return max(a, b);
}

void R() {
  set<int> vset;
  for(int i = 0; i < n; i++)
    vset.insert(i);
  for(int i = 0; i < n; i++) {
    int min_j = *vset.begin();
    for(set<int>::iterator it = vset.begin(); it != vset.end(); ++it)
      if(mx[i][*it] < mx[i][min_j])
	min_j = *it;
    vset.erase(min_j);
    best_path.push_back(min_j);
    best_sum += mx[i][min_j];
  }
}

void tmethod(int depth, int sum, vector<int> path, set<int> vset) {
  if(depth == n) {
    if(sum < best_sum) {
      best_sum = sum;
      best_path = path;
    }
    return;
  }

  for(set<int>::iterator it = vset.begin(); it != vset.end(); ++it) {
    //cout << *it+1 << endl;
    set<int> new_vset = vset;
    new_vset.erase(*it);
    int new_sum = sum + mx[depth][*it];
    int new_depth = depth + 1;
    vector<int> new_path = path;
    new_path.push_back(*it);
    if(new_sum + F0(new_vset, new_depth) < best_sum)
      tmethod(new_depth, new_sum, new_path, new_vset);
  }
}

int main() {
  cin >> n;
  mx = new int *[n];
  for(int i = 0; i < n; i++)
    mx[i] = new int[n];

  for(int i = 0; i < n; i++)
    for(int j = 0; j < n; j++)
      cin >> mx[i][j];
  
  vector<int> path;
  set<int> vset;
  for(int i = 0; i < n; i++) vset.insert(i);


  R();

  cout << "best_sum = " << best_sum << endl;  
  for(vector<int>::iterator it = best_path.begin(); it != best_path.end(); ++it)
    cout << *it+1 << " ";
  cout << endl;


  tmethod(0, 0, path, vset);

  cout << "best_sum = " << best_sum << endl;  
  for(vector<int>::iterator it = best_path.begin(); it != best_path.end(); ++it)
    cout << *it+1 << " ";
  cout << endl;
  return 0;
}
/*
5 
20 31 13 18 10
20 13 15 16 12
20 13 13 12 12
20 13 17 20 12 
20 13 32 30 20
*/
