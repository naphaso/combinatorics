#include <iostream>

using namespace std;

int n;
int *a;

void output() {
  for(int i = 0; i < n; i++)
    cout << a[i] + 1 << " ";
  cout << endl;
}

void lex_rec(int m) {
  if(m == n - 2) {
    output();
    swap(a[n - 1], a[n - 2]);
    output();
    return;
  }

  for(int i = m + 1; i < n; i++) {
    lex_rec(m + 1);
    for(int l = m + 1, r = n - 1; l < r; l++, r--)
      swap(a[l], a[r]);
    swap(a[m], a[i]);
  }
  lex_rec(m + 1);
}

int main() {
  cin >> n;
  a = new int[n];
  for(int i = 0; i < n; i++)
    a[i] = i;
  lex_rec(0);
  return 0;
}
