#include <iostream>
#include <vector>
#include <map>
#include <algorithm>


using namespace std;

int test(int a, int b) {
  return a + b;
}

int main() {
  int w, n;
  cin >> w >> n;
  int *a = new int[n];
  for(int i = 0; i < n; i++)
    cin >> a[i];

  map<int, vector<int> > ss;
  ss[0] = vector<int>();
  for(int i = 0; i < n; i++) {
    map<int, vector<int> > news;
    for(map<int, vector<int> >::iterator it = ss.begin(); it != ss.end(); ++it) {
      if(it->first + a[i] < w) {
	if(ss.count(it->first + a[i]) == 0) {
	  vector<int> sol = it->second;
	  sol.push_back(i);
	  news[it->first + a[i]] = sol;
	}
      } else if(it->first + a[i] == w) {
	
	for(vector<int>::iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2)
	  cout << *it2 + 1 << " ";
	cout << i + 1 << endl;
	return 0;
      }
    }
    ss.insert(news.begin(), news.end());
  }

  return 0;
}
